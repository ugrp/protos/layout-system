# layout-system

Layout system is a simple and minimal HTML component system for laying
out websites.

It is a vocuabulary of elements that you can use to layout a web page.

> This project is an experimental prototype. Not sure if HTML is supposed
  to work like this. It seems to work in Firefox and Chrome.

layout-system = laysys = laysy

It should be easy to use.

## What does it mean?

The following markup gives vertical space to some text.

```
<space vertical=1>
	Some text
<space>
```

There are more components like this one, not a lot.

All of them have a meaninguful use for laying out any web page how you,
or your users would like it.

## It works in markdown editors

Because these components are 100% made of html, if used in a mardown
files (compiled to html such as with static site generators), these
components will be treated as html markup and made a dom element.

# How does it work

This project tries to take advantage of HTML5 and CSS3 latest
features such as flexbox, css variables, and pure html custom
elements (is it even possible?).

To understand how it works, you can open the index.html and see an
example use that renders the home page of this project.

You will see that it imports one only css file, located at
`./styles/index.css`.

The comments in this file should help you understand the available
components and their css implementation.
